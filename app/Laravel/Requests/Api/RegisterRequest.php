<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class RegisterRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'email' => "required|email|max:50|unique_email:0",
            'username' => "required|unique_username:0|username_format",
            'name' => "required|max:50",
            'password' => "required|password_format",
            // 'type'  => "required|valid_account",
        ];


          

        // if($this->has('birthdate')) {
        //     $rules['birthdate'] = "date";
        // }

        // if($this->has('contact_number')) {
        //     $rules['contact_number'] = "phone:PH,mobile";
        //     // $rules['country'] = "required_with:contact_number";
        // }
        return $rules;
    }

    public function messages() {

        return [
            'email.email'           => "Email address format is invalid.",
            'email.unique_email'         => "Email address already taken. Please use another.",
            'username.username_format'   => "Username is invalid. Please try another.",
            'username.unique_username'   => "Username is no longer available. Please try another.",
            'max'                   => "Input value is too long maxlenght is :max",
            'contact_number.phone' => "This mobile number is in invalid format.",
            'password.password_format' => "Your password must be 2-25 characters long only and without space.",
            'type.valid_account'    => "Type of account is invalid.",
            'type.required' => "Please select type of account.",
        ];
    }
}
