<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class RiderRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'name'		=> "required|unique:rider,name,{$id}",
			'location_id'	=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Rider name already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}