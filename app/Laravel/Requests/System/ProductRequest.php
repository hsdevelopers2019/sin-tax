<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProductRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'name'	=> "required|unique:product,name,{$id}",
			'category_id' => "required",
			'uom'	=> "required",
			'price'	=> "required|numeric"
		];

		return $rules;
	}

	public function messages(){
		return [
			'name.unique'	=> "Product name already used. Please double check your input.",
			'required'	=> "Field is required.",
		];
	}
}