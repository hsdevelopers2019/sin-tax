<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WaybillCodRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'cod_encode_date'	=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'cod_encode_date.date'	=> "Invalid date format for Encode date.",
			'cod_encode_date.required'	=> "Delivery date is required.",
		];
	}
}