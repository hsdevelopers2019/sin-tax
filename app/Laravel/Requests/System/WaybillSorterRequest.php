<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WaybillSorterRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'location_id'	=> "required",
			'sorting_date'	=> "required",
			// 'sort_type'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'location_id'	=> "Please indicate the delivery location group.",
			'rider_id.required'	=> "Please choose a rider before you continue.",
		];
	}
}