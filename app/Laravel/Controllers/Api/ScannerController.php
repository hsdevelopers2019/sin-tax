<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\WishlistTransaction;
use App\Laravel\Models\WishlistViewer;
use App\Laravel\Notifications\PusherNotification;
use App\Laravel\Transformers\GeneralRequestTransformer;
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\WishlistTransactionTransformer;
use App\Laravel\Transformers\WishlistViewerTransformer;
use App\Laravel\Transformers\ScannerTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\laravel\models\Product;

use Str, Carbon, DB, Helper;

class ScannerController extends Controller
{

	protected $data = array();

    public function __construct() {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => FALSE,
            'status_code' => "BAD_REQUEST"
            );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }


    public function scan(Request $request)
    {

       
        $qr_code  = $request->get('qr_code');
    

        if(Product::where('qr_code','=',$qr_code)->count() > 0)
        {
            $this->response['msg'] = "PRODUCT";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;

            $scanner = Product::where('qr_code','=',$qr_code)->get();

        

        $this->response['data'] = $this->transformer->transform($scanner, new ScannerTransformer, 'collection')[0];

        $this->response_code = 200;

        return response()->json($this->response,$this->response_code);
        }
        else
        {
            $this->response['data'] = array(['message' =>'no data found'])[0];
            
            $this->response['msg'] = "QR LIST";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;
            return response()->json($this->response,$this->response_code);

        }



        
    }

 
}
