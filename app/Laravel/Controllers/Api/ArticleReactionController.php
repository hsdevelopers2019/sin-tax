<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\User;
use App\Laravel\Models\Article;
use App\Laravel\Models\ArticleReaction;
use Illuminate\Http\Request;
use App\Laravel\Transformers\ArticleReactionTransformer;
use App\Laravel\Transformers\TransformerManager;

use App\Laravel\Jobs\Article\NotifyParticipants;
use App\Laravel\Notifications\Self\Article\NewReactionNotification as SelfNewReactionNotification;

class ArticleReactionController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {

		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $article = $request->get('article_data');
        $user = $request->user();

        $this->response['msg'] = "List of likers";

        $reactions = ArticleReaction::where('is_active','yes')->where('article_id',$article->id)->orderBy('updated_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_LIKERS";
        $this->response['has_morepages'] = $reactions->hasMorePages();
        $this->response['data'] = $this->transformer->transform($reactions, new ArticleReactionTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function react(Request $request, $format = '') {

        $user = $request->user();
        $article = $request->get('article_data');
        $reaction = ArticleReaction::where('user_id',$user->id)->where('article_id',$article->id)->first();

        if($reaction){
            if($reaction->is_active == "yes"){
                $this->response['msg'] = "Article unliked.";
                $this->response['status_code'] = "UNLIKED_ARTICLE";
                $reaction->is_active = "no";
            }else{
                $this->response['msg'] = "Article liked.";
                $this->response['status_code'] = "LIKED_ARTICLE";
                $reaction->is_active = "yes";
            }
        }else{
            $reaction = new ArticleReaction;
            $reaction->user_id = $user->id;
            $reaction->article_id = $article->id;
            $reaction->type = "like";
            $reaction->is_active = "yes";
            $this->response['msg'] = "Article liked.";
            $this->response['status_code'] = "LIKED_ARTICLE";
            $owner = User::find($article->user_id);
            $owner->notify(new SelfNewReactionNotification($user,$article));
            dispatch(new NotifyParticipants($user, $article,"NEW_REACTION"));
        }

        $reaction->save();

        $this->response['status'] = TRUE;
        $this->response['data'] = $this->transformer->transform($reaction, new ArticleReactionTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

}