<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\AndriodVersionControl;
use App\Laravel\Models\IOSVersionControl;
use App\Laravel\Models\ArticleCategory;
use App\Laravel\Models\specialty;
use App\Laravel\Transformers\ArticleCategoryTransformer;
use App\Laravel\Transformers\SpecialtyTransformer;

use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use Illuminate\Http\Request;
use Str, Carbon, DB, Helper;

class AppSettingController extends Controller
{

	protected $data = array();

    public function __construct() {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => FALSE,
            'status_code' => "BAD_REQUEST"
            );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function version(Request $request, $format = '') {

        $data = array();

        $android_version = AndriodVersionControl::orderBy('created_at', "DESC")->first() ? : new AndriodVersionControl;

        $ios_version = IOSVersionControl::orderBy('created_at', "DESC")->first() ? : new IOSVersionControl;

        $data['ios_version'] = $ios_version;
        $data['ios_version']['is_maintenance'] = $ios_version->is_maintenance == "yes";
        if($data['ios_version']['is_maintenance']) {
             $data['ios_version']['next_update'] = $ios_version->created_at
                                                        ->addHours($ios_version->maintenance_counter);
        }
        
        $data['android_version'] = $android_version;
        $data['android_version']['is_maintenance'] = $android_version->is_maintenance == "yes";
        if($data['android_version']['is_maintenance']) {
             $data['android_version']['next_update'] = $android_version->created_at
                                                        ->addHours($android_version->maintenance_counter);
        }

        $this->response['msg'] = "App Settings";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "APP_SETTINGS";
        $this->response['data'] = $this->transformer->transform($data, new MasterTransformer, 'item');
        // $this->response['categories'] = $this->transformer->transform($categories,new ArticleCategoryTransformer,'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function category(Request $request, $format = '') {
        $data = ArticleCategory::where('status','active')->get();

        $this->response['msg'] = "List of category";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CATEGORY_SETTING";
        $this->response['data'] = $this->transformer->transform($data, new ArticleCategoryTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function specialty(Request $request, $format = '') {
        $data = Specialty::where('status','active')->get();

        $this->response['msg'] = "List of specialty";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "SPECIALTY_SETTING";
        $this->response['data'] = $this->transformer->transform($data, new SpecialtyTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }


}
