<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\User;
use App\Laravel\Models\Views\Chat;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\ChatConversation;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ChatConversationRequest;
use App\Laravel\Transformers\ChatTransformer;
use App\Laravel\Transformers\ChatParticipantTransformer;
use App\Laravel\Transformers\ChatConversationTransformer;
use App\Laravel\Transformers\TransformerManager;

use App\Laravel\Notifications\Self\Chat\JoinChatNotification;
use App\Laravel\Notifications\Self\Chat\PublicJoinChatNotification;
use App\Laravel\Jobs\Chat\NotifyParticipants;

class ChatParticipantController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {
        $chat = $request->get('chat_data');
		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$chat->title} Participants";

        // $sort_by = $request->get('sort')

        $members = ChatParticipant::where('chat_id',$chat->id)->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_PARTICPANTS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new ChatParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function moderator(Request $request, $format = '') {
        $chat = $request->get('chat_data');
        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$chat->title} Moderators";

        // $sort_by = $request->get('sort')

        $members = ChatParticipant::where('chat_id',$chat->id)->where('role','moderator')->orderBy('id',"ASC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_MODERATORS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new ChatParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }


    public function member(Request $request, $format = '') {
        $chat = $request->get('chat_data');
        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$chat->title} Members";

        // $sort_by = $request->get('sort')

        $members = ChatParticipant::where('chat_id',$chat->id)->where('role','member')->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_MEMBERS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new ChatParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(Request $request, $format = '') {

        $auth = $request->user();

        $chat = $request->get('chat_data');
        $user = $request->get('user_data');

        $is_moderator = ChatParticipant::where('chat_id',$chat->id)
                                        ->where('user_id',$auth->id)
                                        ->where('role','moderator')->first();

        // if(!$is_moderator){
        //     $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
        //     $this->response['status'] = FALSE;
        //     $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
        //     $this->response_code = 403;
        //     goto callback;
        // }

        $is_participant = ChatParticipant::where('chat_id',$chat->id)
                                        ->where('user_id',$user->id)->first();
        if($is_participant){
            $this->response['msg'] = "{$user->name} is already in the group.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "ALREADY_PARTICIPANT";
            $this->response_code = 400;
            goto callback;
        }

        $member = new ChatParticipant;
        $member->user_id = $user->id;
        $member->chat_id = $chat->id;
        $member->role = "member";
        $member->save();

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$user->name} added to the group.";
        $message->save();
        
        dispatch(new NotifyParticipants($auth, $chat,"NEW_MEMBER",[$auth->id,$user->id]));
        $user->notify(new JoinChatNotification($chat,$auth));


        $this->response['msg'] = "Added new participant.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "NEW_PARTICIPANT";
        $this->response['data'] = $this->transformer->transform($member, new ChatParticipantTransformer, 'item');
        $this->response_code = 201;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function promote(Request $request, $format = '') {
        $auth = $request->user();
        $chat = $request->get('chat_data');
        $user = $request->get('user_data');

        if($request->get('user_id') == "1"){$user = new User; $user->id = 1;}

        $chat_participant = $request->get('chat_participant_data');


        if($chat->owner_user_id != $auth->id){
            $is_moderator = ChatParticipant::where('chat_id',$chat->id)
                                            ->where('user_id',$auth->id)
                                            ->where('role','moderator')->first();

            if(!$is_moderator){
                $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
                $this->response['status'] = FALSE;
                $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
                $this->response_code = 403;
                goto callback;
            }
        }

        if(in_array($chat_participant->user_id,[$chat->owner_user_id,1]) OR $chat_participant->role == "moderator"){
            $this->response['msg'] = "No more action is required in the selected user.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "NO_ACTION_REQUIRED";
            $this->response_code = 400;
            goto callback;
        }   

        $chat_participant->role = "moderator";
        $chat_participant->save();

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$user->name} promoted as moderator in the group.";
        $message->save();

        $this->response['msg'] = "{$user->name} promoted as moderator in the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "NEW_MODERATOR";
        $this->response['data'] = $this->transformer->transform($chat_participant, new ChatParticipantTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function demote(Request $request, $format = '') {
        $auth = $request->user();
        $chat = $request->get('chat_data');
        $user = $request->get('user_data');
        $chat_participant = $request->get('chat_participant_data');

        if($request->get('user_id') == "1"){$user = new User; $user->id = 1;}


        if($chat->owner_user_id != $auth->id){
            $is_moderator = ChatParticipant::where('chat_id',$chat->id)
                                            ->where('user_id',$auth->id)
                                            ->where('role','moderator')->first();

            if(!$is_moderator){
                $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
                $this->response['status'] = FALSE;
                $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
                $this->response_code = 403;
                goto callback;
            }
        }
        

        if(in_array($chat_participant->user_id,[$chat->owner_user_id,1]) OR $chat_participant->role == "member"){
            $this->response['msg'] = "No more action is required in the selected user.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "NO_ACTION_REQUIRED";
            $this->response_code = 400;
            goto callback;
        }   

        $chat_participant->role = "member";
        $chat_participant->save();

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$user->name} demoted as member in the group.";
        $message->save();

        $this->response['msg'] = "{$user->name} demoted to member role in the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "DEMOTED_MEMBER";
        $this->response['data'] = $this->transformer->transform($chat_participant, new ChatParticipantTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }


    public function leave(Request $request, $format = '') {
        $auth = $request->user();
        $chat = $request->get('chat_data');

        $chat_participant = ChatParticipant::where('chat_id',$request->get('chat_id'))->where('user_id',$auth->id)->first();

        if(!$chat_participant){
            $this->response = [
                'msg' => "You're not participant in the group.",
                'status' => FALSE,
                'status_code' => "NOT_PARTICIPANT",
                'hint' => "Make sure your part of the group."
            ];
            $this->response_code = 401;
            goto callback;
        }

        if($chat->owner_user_id == $auth->id){
            $chat->status = 'inactive';
            $chat->save();

            $message = new ChatConversation;
            $message->chat_id = $chat->id;
            $message->type = "announcement";
            $message->sender_user_id = 1;
            $message->content = "Group has been closed.";
            $message->save();

            $this->response['msg'] = "Group has been successfully closed.";
            $this->response['status'] = TRUE;
            $this->response['status_code'] = "GROUP_CLOSED";
            $this->response_code = 200;

        }else{
            $chat_participant->delete();

            $message = new ChatConversation;
            $message->chat_id = $chat->id;
            $message->type = "announcement";
            $message->sender_user_id = 1;
            $message->content = "{$auth->name} left the group.";
            $message->save();

            $this->response['msg'] = "You successfully leave the group";
            $this->response['status'] = TRUE;
            $this->response['status_code'] = "GROUP_LEFT";
            $this->response_code = 200;
        }

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {
        $auth = $request->user();
        $chat = $request->get('chat_data');
        $user = $request->get('user_data');
        $chat_participant = $request->get('chat_participant_data');

        if($request->get('user_id') == "1"){$user = new User; $user->id = 1;}


        if($chat->owner_user_id != $auth->id){
            $is_moderator = ChatParticipant::where('chat_id',$chat->id)
                                            ->where('user_id',$auth->id)
                                            ->where('role','moderator')->first();

            if(!$is_moderator){
                $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
                $this->response['status'] = FALSE;
                $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
                $this->response_code = 403;
                goto callback;
            }
        }
        

        if(in_array($chat_participant->user_id,[$chat->owner_user_id,1])){
            $this->response['msg'] = "No more action is required in the selected user.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "NO_ACTION_REQUIRED";
            $this->response_code = 400;
            goto callback;
        }   

        $chat_participant->delete();

        $message = new ChatConversation;
        $message->chat_id = $chat->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$user->name} removed from the group.";
        $message->save();

        $this->response['msg'] = "{$user->name} successfully remove from the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "REMOVED_PARTICIPANT";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}