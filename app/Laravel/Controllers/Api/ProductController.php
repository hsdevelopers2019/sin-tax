<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Models\WishlistTransaction;
use App\Laravel\Models\WishlistViewer;
use App\Laravel\Notifications\PusherNotification;
use App\Laravel\Transformers\GeneralRequestTransformer;
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\WishlistTransactionTransformer;
use App\Laravel\Transformers\WishlistViewerTransformer;
use App\Laravel\Transformers\ProductTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\laravel\models\Product;

use Str, Carbon, DB, Helper;

class ProductController extends Controller
{

	protected $data = array();

    public function __construct() {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => FALSE,
            'status_code' => "BAD_REQUEST"
            );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }


    public function get_product(Request $request, $format = '')
    {

        // $brand = $request->get('brand');
        $id  = $request->get('id');

            $this->response['msg'] = "This Product is Authentic";
            $this->response['status_code'] = "this is test";
            $this->response['status'] = TRUE;

        if(Product::where('id','=',$id)->count() > 0)
        {
          
            $product = Product::where('id','=',$id)->get();

            $this->response['data'] = $this->transformer->transform($product, new ProductTransformer,'collection')[0];
            
            $this->response_code = 200;

            return response()->json($this->response,$this->response_code);
        }
        else
        {
            $this->response['msg'] = "NO Data found";
            $this->response['status_code'] = "this is test";
            $this->response['status'] = TRUE;
            return response()->json($this->response,$this->response_code);

        }



        
    }

    public function get_all_product(Request $request)
    {
        if (Product::all()->count() == 0) 
        {
            $this->response['msg'] = "PRODUCT LIST";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;
            $this->response['data'] = array('message' => 'No Product found' );
            return response()->json($this->response,$this->response_code);
        }
        else
        {
            $this->response['msg'] = "PRODUCT LIST";
            $this->response['status_code'] = "LIST_OF_QR";
            $this->response['status'] = TRUE;

            $products = Product::get();

            $this->response['data'] = $this->transformer->transform($products, new ProductTransformer, 'collection');
            $this->response_code = 200;
            return response()->json($this->response,$this->response_code);
        }
    }

 
}
