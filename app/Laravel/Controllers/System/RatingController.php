<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Rating;
/**
*
* Requests used for validating inputs
*/

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Input;

class RatingController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['heading'] = "Ratings";
	}

	public function index () {
		$this->data['page_title'] = " :: Ratings - Record Data";
		$this->data['ratings'] = Rating::orderBy('created_at',"DESC")->paginate(15);
		return view('system.ratings.index',$this->data);
	}

}