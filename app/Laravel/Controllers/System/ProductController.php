<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Product;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\SpecialtyRequest;

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str;

class ProductController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ 'active' => "Active","inactive" => "Inactive"];
		$this->data['heading'] = "Expertise";
	}

	public function index () {
		$this->data['page_title'] = "Product";
		$this->data['specialties'] = Product::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.product.index',$this->data);
	}

	public function create () {
		$this->data['page_title'] = "Add new Product";
		// $this->data['manufacture'] = Product::get();
		return view('system.product.create',$this->data);
	}

	public function show()
	{
		$this->data['page_title'] = "Product";
		$this->data['products'] = Product::orderBy('id',"DESC")->paginate(15);
		return view('system.product.show',$this->data);
	}

	public function store (SpecialtyRequest $request) {
		// try {
		// 	$new_specialty = new Specialty;
		// 	$new_specialty->fill($request->only('name'));
		// 	$new_specialty->status = $request->get('status');
		// 	$new_specialty->user_id = $request->user()->id;
		// 	if($new_specialty->save()) {
		// 		session()->flash('notification-status','success');
		// 		session()->flash('notification-msg',"New record has been added.");
		// 		return redirect()->route('system.specialty.index');
		// 	}
		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg','Something went wrong.');

		// 	return redirect()->back();
		// } catch (Exception $e) {
		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg',$e->getMessage());
		// 	return redirect()->back();
		// }
	}

	public function edit ($id = NULL) {
		// $this->data['page_title'] = " :: Specialties - Edit record";
		// $specialty = Specialty::find($id);

		// if (!$specialty) {
		// 	session()->flash('notification-status',"failed");
		// 	session()->flash('notification-msg',"Record not found.");
		// 	return redirect()->route('system.specialty.index');
		// }

		// if($id < 0){
		// 	session()->flash('notification-status',"warning");
		// 	session()->flash('notification-msg',"Unable to update special record.");
		// 	return redirect()->route('system.specialty.index');	
		// }

		// $this->data['specialty'] = $specialty;
		// return view('system.specialty.edit',$this->data);
	}

	public function update (SpecialtyRequest $request, $id = NULL) {
		// try {
		// 	$specialty = Specialty::find($id);

		// 	if (!$specialty) {
		// 		session()->flash('notification-status',"failed");
		// 		session()->flash('notification-msg',"Record not found.");
		// 		return redirect()->route('system.specialty.index');
		// 	}

		// 	if($id < 0){
		// 		session()->flash('notification-status',"warning");
		// 		session()->flash('notification-msg',"Unable to update special record.");
		// 		return redirect()->route('system.specialty.index');	
		// 	}

		// 	$specialty->fill($request->only('name'));
		// 	$specialty->status = $request->get('status');
		// 	$specialty->user_id = $request->user()->id;
			

		// 	if($specialty->save()) {
		// 		session()->flash('notification-status','success');
		// 		session()->flash('notification-msg',"Record has been modified successfully.");
		// 		return redirect()->route('system.specialty.index');
		// 	}

		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg','Something went wrong.');

		// } catch (Exception $e) {
		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg',$e->getMessage());
		// 	return redirect()->back();
		// }
	}

	public function destroy ($id = NULL) {
		// try {
		// 	$specialty = Specialty::find($id);

		// 	if (!$specialty) {
		// 		session()->flash('notification-status',"failed");
		// 		session()->flash('notification-msg',"Record not found.");
		// 		return redirect()->route('system.specialty.index');
		// 	}

		// 	if($id < 0){
		// 		session()->flash('notification-status',"warning");
		// 		session()->flash('notification-msg',"Unable to remove special record.");
		// 		return redirect()->route('system.specialty.index');	
		// 	}

		// 	if($specialty->delete()) {
		// 		session()->flash('notification-status','success');
		// 		session()->flash('notification-msg',"Record has been deleted.");
		// 		return redirect()->route('system.specialty.index');
		// 	}

		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg','Something went wrong.');

		// } catch (Exception $e) {
		// 	session()->flash('notification-status','failed');
		// 	session()->flash('notification-msg',$e->getMessage());
		// 	return redirect()->back();
		// }
	}

}
