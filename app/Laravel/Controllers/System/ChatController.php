<?php 

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Views\Chat;
use App\Laravel\Models\ChatConversation;

/**
*
* Requests used for validating inputs
*/

/**
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str,Input;

class ChatController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = ['' => "Choose Location Type", 'metro_manila' => "Metro Manila","provincial" => "Provincial"];
		$this->data['heading'] = "Group Chats";
	}

	public function index () {
		$this->data['page_title'] = " :: Group Chats - Record Data";
		$this->data['chats'] = Chat::orderBy('latest_message_created_at',"DESC")->paginate(15);
		return view('system.chat.index',$this->data);
	}

	

	public function show ($id = NULL) {
		$chat = Chat::find($id);
		$per_page = Input::get('per_page',10);
		if (!$chat) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.chat.index');
		}

		$this->data['page_title'] = " :: Group Chats - {$chat->title}";

		$this->data['chat'] = $chat;
		$this->data['conversation'] = ChatConversation::where('chat_id',$chat->id)->orderBy('created_at',"DESC")->paginate($per_page);
		return view('system.chat.show',$this->data);
	}

}