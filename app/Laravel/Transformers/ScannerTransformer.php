<?php

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class ScannerTransformer extends TransformerAbstract{

    protected $availableIncludes = [
    ];


    public function transform(Product $Scanner) {

        return [
            'id' => $Scanner->id ?:0,
            'brand' => $Scanner->brand,
            'description' => $Scanner->description,
            'qr_code' => $Scanner->qr_code,
            'manufacturer' => $Scanner->manufacturer,
            'distributor' => $Scanner->distributor,
            
         ];
    }
}