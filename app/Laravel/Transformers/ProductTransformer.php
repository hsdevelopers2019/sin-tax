<?php

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Product;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class ProductTransformer extends TransformerAbstract{

    protected $availableIncludes = [
    ];


    public function transform(Product $Product) {

        return [
            'id' => $Product->id ?:0,
            'brand' => $Product->brand,
            'description' => $Product->description,
            'qr_code' => $Product->qr_code,
            'distributor' => $Product->distributor,
            'manufacturer' => $Product->manufacturer
            
         ];
    }

   
}