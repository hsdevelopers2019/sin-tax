@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Product List<span class="panel-subtitle">Product Information.</span></div>
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <th>Product Name</th>
              <th>Product Description</th>
              <th>Product Code</th>
              <th>Product Manufacuturer</th>
              <th>Product Distributor</th>
            </thead>
            @foreach($products as $product)
            <tr>
              <td>{{$product->brand}}</td>
              <td>{{$product->description}}</td>
              <td>{{$product->qr_code}}</td>
              <td>{{$product->manufacturer}}</td>
              <td>{{$product->distributor}}</td>
            
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


