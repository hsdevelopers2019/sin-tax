@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Update Record Form<span class="panel-subtitle">Modify specialty information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Specialty</label>
              <input type="text" placeholder="Specialty" class="form-control" name="name" value="{{old('name',$specialty->name)}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
              <label>Status</label>
              {!!Form::select('status',$statuses,old('status',$specialty->status),['class' => "form-control"])!!}
              @if($errors->first('status'))
              <span class="help-block">{{$errors->first('status')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Update Record</button>
                  <a href="{{route('system.product.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


