@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-12">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Add new Product<span class="panel-subtitle">Product Information.</span></div>
        <div class="panel-body">
          <form method="POST" action="">
            {!!csrf_field()!!}
            
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Product Name</label>
              <input type="text" placeholder="name" class="form-control" name="product_name" value="{{old('product_name')}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('product_name')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Product Description</label>
              <input type="text" placeholder="Description" class="form-control" name="product_description" value="{{old('product_description')}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('product_description')}}</span>
              @endif
            </div>
            <div class="form-group {{$errors->first('name') ? 'has-error' : NULL}}">
              <label>Product Code</label>
              <input type="text" placeholder="Code" class="form-control" name="product_code" value="{{old('product_code')}}">
              @if($errors->first('name'))
              <span class="help-block">{{$errors->first('product_code')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('manufacturer') ? 'has-error' : NULL}}">
              <label>Manufacturer</label>
              {!!Form::select('manufacturer',['','Manufacturer1','Manufacturer2'],old('status','active'),['class' => "form-control"])!!}
              @if($errors->first('status'))
              <span class="help-block">{{$errors->first('status')}}</span>
              @endif
            </div>

            <div class="form-group {{$errors->first('distributor') ? 'has-error' : NULL}}">
              <label>Distributor</label>
              {!!Form::select('distributor',['','puregold','savemore'],old('status','active'),['class' => "form-control"])!!}
              @if($errors->first('status'))
              <span class="help-block">{{$errors->first('status')}}</span>
              @endif
            </div>
            
            <div class="row xs-pt-15">
              <div class="col-xs-6">
                  <button type="submit" class="btn btn-space btn-success">Add Product</button>
                  <a href="{{route('system.product.index')}}" class="btn btn-space btn-default">Cancel</a>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


