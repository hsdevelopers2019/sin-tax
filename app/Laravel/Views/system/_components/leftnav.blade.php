<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle"></a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Quick Menu</li>
                        <li><a href="{{route('system.dashboard')}}"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a> </li>
                        
                        <li class="divider">Product Management</li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-attachment"></i><span>Products</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{route('system.product.create')}}">Add Product</a> </li>
                                <li><a href="{{route('system.product.show')}}">View all Products</a> </li>
                             
                            </ul>
                        </li>
                            
                        <li class="divider">User Management</li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-attachment"></i><span>Users</span></a>
                            <ul class="sub-menu">
                                <li><a href="">Manufacturer/Importer</a> </li>
                                <li><a href="">Distributor</a> </li>
                                <li><a href="">Retailer</a> </li>
                                <li><a href="">Consumer</a> </li>
                            </ul>
                        </li>
                 

                        

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>