@extends('system._layouts.main')
@section('content')


<div class="col-md-12">
<div class="main-content container-fluid">
    <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong>Personal Information</strong></a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <!---start--> 
                        <form method="POST" action="" enctype="multipart/form-data">
                            {!!csrf_field()!!}
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Title</label>
                                        <select class="form-control" name="region">
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Engr">Engr</option>
                                            <option value="Phd">Phd</option>
                                            <option value="Atty">Atty</option>
                                            <option value="Dr">Dr</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>First Name</label>
                                        <input type="text" placeholder="First Name" class="form-control" name="" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Middle Name</label>
                                        <input type="text" placeholder="Middle Name" class="form-control" name="">
                                    </div>
                                    <div class="col-md-3">
                                        <label>Last Name</label>
                                        <input type="text" placeholder="Last Name" class="form-control" name="" >
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"> 
                                        <label>Name on ID</label>
                                        <input type="text" placeholder="Name on ID" class="form-control" name="name_id">
                                    </div>
                                    <div class="col-md-3"> 
                                        <label>Birthday</label>
                                        <input type="date" placeholder="Birthday" class="form-control" name="birthday" >
                                    </div> 
                                    <div class="col-sm-3">
                                        <label>Age</label>
                                        <input type="number" placeholder="Age" class="form-control" name="age" >
                                    </div> 
                                    <div class="col-sm-3">
                                        <label>Gender</label>
                                        <div class="radio">
                                            <label class="radio-inline"><input type="radio" name="gender" value="option1">Male</label>
                                            <label class="radio-inline"><input type="radio" name="gender" value="option2">Female</label>
                                        </div>
                                    </div>    
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Mobile Number</label>
                                        <input type="text" placeholder="Email" class="form-control" name="" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Landline Number</label>
                                        <input type="text" placeholder="Area Code + No." class="form-control" name="" >
                                     </div>
                                     <div class="col-md-3">   
                                        <label>Email Address</label>
                                        <input type="email" placeholder="Email Address" class="form-control" name="" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Home Address</label>
                                        <input type="text" placeholder="Home Address" class="form-control" name="" >
                                    </div>
                                </div>   
                                    <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Municipality / City</label>
                                        <input type="text" placeholder="Muncipality / City" class="form-control" name="province" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Province</label>
                                        <input type="text" placeholder="Province" class="form-control" name="province" >
                                    </div>
                                    <div class="col-md-3">
                                    <label>Region</label>
                                        <select class="form-control" name="region">
                                            <option value="NCR" selected="selected">NCR</option>
                                            <option value="Region1">Region 1</option>
                                            <option value="Region2">Region 2</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Religion</label>
                                        <select class="form-control" name="region">
                                            <option value="Catholic" selected="selected">Catholic</option>
                                            <option value="Christian">Christian</option>
                                            <option value="IglesianiCristo">Iglesia ni Cristo</option>
                                            <option value="LatterDaySaints">Latter Day Saints</option>
                                            <option value="Jehova's Witnesses">Jehova's Witnesses</option>
                                            <option value="Muslim">Muslim</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">    
                                    <div class="col-md-3"> 
                                        <label>Business Name</label>
                                        <input type="text" placeholder="Business Name" class="form-control" name="businessaddress" >
                                    </div>
                                    <div class="col-md-3">
                                        <label>Business Address</label>
                                        <input type="text" placeholder="Business Address " class="form-control" name="preffered_schedule" > 
                                    </div> 
                                    <div class="col-md-3">                                             
                                        <label>Program Inclusions</label>
                                        <select class="form-control" name="region">
                                            <option value="KKME" selected="selected">KKME</option>
                                            <option value="KAMMP">KAMMP</option>
                                            <option value="BOTH">BOTH</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Profile Picture</label>
                                        <input type="file" placeholder="Profile pic" class="form-control" name="file" >
                                     </div>
                                </div>
                                <br>
                                <div class="row">
                                     <div class="col-sm-3">
                                        <label>Preffered Schedule</label>
                                        <input type="text" placeholder="Eg: Monday- Wednesday" class="form-control" name="" >
                                     </div>
                                </div>
                                <hr>
                                <!---start--->
                                <h4><strong>Educational Background</strong></h4>
                                 <hr>
                                 <table class="table table-bordered" id="">
                                        <label>MASTERS OF BUSINESS ADMINISTRATION</label>
                                            <tr>
                                                <td><input type="text" name="name[]" placeholder="Name of School" class="form-control name_list" /></td>
                                                <td><input type="text" name="name[]" placeholder="Address" class="form-control name_list" /></td>
                                                 <td><input type="text" name="name[]" placeholder="Other Information" class="form-control name_list" /></td>
                                            </tr>
                                        <form name="add_name" id="add_name">
                                            <div class="table-responsive">
                                                <table class="table table-bordered" id="dynamic_field">
                                                    <label>UNIVERSITY / COLLEGE</label>
                                                        <tr>
                                                            <td><input type="text" name="name[]" placeholder="Name of School" class="form-control name_list" /></td>
                                                            <td><input type="text" name="name[]" placeholder="Address" class="form-control name_list" /></td>
                                                             <td><input type="text" name="name[]" placeholder="Other Information" class="form-control name_list" /></td>
                                                            <td align="center"><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
                                                        </tr>
                                                </table>
                                               
                                            </div>
                                        </form>
                                    </table>
                                    <!---end-->
                                    <hr>
                                     <h4><strong>Organization / Affiliation</strong></h4>
                                    <hr>
                                    <!--start-->
                                    <form method="POST" action="">
                                        <input type="hidden" name="_token" value="DZbobBlCnaaRQ2oB6PPfM9uiNwAhSifnCgSr28L6">
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>NGO</label>
                                                        <input type="text" placeholder="NGO" class="form-control" name="name" value=""><br>      
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Memberships</label>
                                                        <input type="text" placeholder="Memberships" class="form-control" name="name" value=""><br>      
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Founded Organizations</label>
                                                        <input type="text" placeholder="Founded Organizations" class="form-control" name="name" value=""><br>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label>Community Organizations</label>
                                                        <input type="text" placeholder="Community Organizations" class="form-control" name="name" value=""><br>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Professional Organizations</label>
                                                        <input type="text" placeholder="Professional Organizations" class="form-control" name="" value=""><br>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Others</label>
                                                        <input type="text" placeholder="Others" class="form-control" name="name" value=""><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!--end-->
                                        <hr>
                                         <h4><strong>Working Business Expertise</strong></h4>
                                        <hr>
                                        <!---start-->
                                         <div class="form-group">
                                            <form name="add_name" id="add_name">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered" id="dynamic_field2">
                                                        <label>Working Business Expertise</label>
                                                        <tr>
                                                            <td><input type="text" name="name[]" placeholder="Position" class="form-control name_list" /></td>
                                                            <td><input type="text" name="name[]" placeholder="Business Name" class="form-control name_list" /></td>
                                                             <td><input type="text" name="name[]" placeholder="Dates Inclusion" class="form-control name_list" /></td>
                                                             <td><input type="text" name="name[]" placeholder="Responsibilities" class="form-control name_list" /></td>
                                                            <td><button type="button" name="add" id="add2" class="btn btn-success">Add More</button></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                        <!--end-->
                                        <hr>
                                         <h4><strong>Areas of Expertise/Fields Specialization</strong></h4>
                                        <hr>
                                        <!---start-->
                                        <div class="form group">
                                        <div class="col-12 col-sm-8 col-lg-6 form-check mt-1">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" checked="" id="check6">
                                                      <label class="custom-control-label" for="check6">Entrep Mindset</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check7">
                                                      <label class="custom-control-label" for="check7">HR Management</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Operations Management</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Business Model Canvas</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Marketing</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" checked="" id="check6">
                                                      <label class="custom-control-label" for="check6">Coop Management</label>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-8 col-lg-6 form-check mt-1">
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check7">
                                                      <label class="custom-control-label" for="check7">Business Law / Obligations and Contracts</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Taxation</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Accounting and Finance</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Human Resource Management</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Supply and Value Chain</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                      <input class="custom-control-input" type="checkbox" id="check8">
                                                      <label class="custom-control-label" for="check8">Product Development</label>
                                                    </div>
                                                </div>
                                        <div class="row">
                                           <div class="col-md-12">
                                            <hr>
                                             <h4><strong>Previous Training(s) Conducted </strong></h4>
                                            <hr>
                                             <!---start-->
                                            <textarea class="form-control" id="inputTextarea3"></textarea>
                                             <!--end-->
                                           </div>
                                           <div class="col-md-12">
                                            <hr>
                                             <h4><strong>Short Introduction / Profile Summary</strong></h4>
                                            <hr>
                                             <!---start-->
                                            <textarea class="form-control" id="inputTextarea3"></textarea>
                                             <!--end-->
                                           </div>
                                           <div class="col-md-12">
                                            <hr>
                                             <h4><strong>Other Input / Additional</strong></h4>
                                            <hr>
                                             <!---start-->
                                                 <!---start-->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <label><strong>Industry</strong></label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" checked="" id="check3">
                                                          <label class="custom-control-label" for="check3">Realstate</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" id="check4">
                                                          <label class="custom-control-label" for="check4">food</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" id="check5">
                                                          <label class="custom-control-label" for="check5">follow</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" checked="" id="check3">
                                                          <label class="custom-control-label" for="check3">Realstate</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" id="check4">
                                                          <label class="custom-control-label" for="check4">food</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox">
                                                          <input class="custom-control-input" type="checkbox" id="check5">
                                                          <label class="custom-control-label" for="check5">follow</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Mentor Category</label>
                                                <select class="form-control" name="status">
                                                <option value="active" selected="selected">Entrepreneurs</option>
                                                <option value="inactive">Business Practitioners</option>
                                                <option value="inactive">Academe</option>
                                                </select><br>                            
                                            </div>
                                            <div class="col-md-6">
                                                <label>Mentor Levels</label>
                                                <select class="form-control" name="status" id="chk" >
                                                <option value="inactive">Level-1 Entrepreneurship Mind setting</option>
                                                <option value="lvl2" >Level-2 Specific Topics</option>
                                                <option value="inactive">Level-3 Panelist</option>
                                                </select><br>
                                            </div>
                                         </div>
                                         <div class="row">
                                            <div class="col-md-4">
                                                <div id="holder"></div>
                                            </div>
                                            <div class="col-lg-4">
                                                  <div id="holder2"></div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div id="holder3"></div>
                                            </div>
                                         </div>
                                       
                                         <div class="row">
                                          
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Cluster Options</label>
                                                <select class="form-control" name="status">
                                                    <option value="active" selected="selected">1</option>
                                                    <option value="inactive">2</option>
                                                    <option value="inactive">3</option>
                                                    <option value="inactive">4</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Regions</label>
                                                <select class="form-control" name="status">
                                                    <option value="active" selected="selected">Ilocos Region</option>
                                                    <option value="inactive">Cagayan Valley</option>
                                                    <option value="inactive">Central Luzon</option>
                                                    <option value="inactive">Calabarzon</option>
                                                    <option value="inactive">Mimaropa</option>
                                                    <option value="inactive">Bicol Region</option>
                                                    <option value="inactive">Cordillera Administrative Region (CAR)</option>
                                                    <option value="inactive">National Capital Region (NCR)</option>
                                                    <option value="inactive">Western Visayas</option>
                                                    <option value="inactive">Central Visayas</option>
                                                    <option value="inactive">Eastern Visayas</option>
                                                    <option value="inactive">Zambaoanga Peninsula</option>
                                                    <option value="inactive">Northern Mindanao</option>
                                                    <option value="inactive">Davao Region</option>
                                                    <option value="inactive">Soccsksargen</option>
                                                    <option value="inactive">Caraga</option>
                                                    <option value="inactive">Autonomous Region of Muslim Mindanao</option>
                                                </select><br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"><br>
                                                <label>Upload Code of Conduct File</label>
                                                <input type="file" class="form-control form-file" name="file" value=""><br>
                                                <label>Other Related Files <i>Videos, Intro, PPT</i></label>
                                                <input type="file" class="form-control form-file" name="file" value=""><br>
                                                 <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit"><br>
                                            </div>
                                            <div class="col-md-3"><br>
                                                <label>Upload TOR File</label>
                                                <input type="file" class="form-control form-file" name="file" value=""><br>
                                            </div>
                                            <div class="col-md-3"><br>
                                                <label>Upload Photo 400x400px <i>/JPEG/PNG</i></label>
                                                <input type="file" class="form-control form-file" name="file" value=""><br><br>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Upload Presentation Samples <i>PPT, PPTX, or PDF</i></label>
                                                <input type="file" class="form-control form-file" name="file" value=""><br>
                                            </div>
                                        </div>                    
                                    </div>
                                   <!--end-->
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                        <!--end of first panel-->
                  
                      
                           
                                
                          
                           
                                
                                
                                </div><!--upperdivs-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $("#chk").change(function(){
            if($(this).val()=='lvl2'){
                //var $ctrl = $('<input/>').attr({ type: 'checkbox', name:'chk'}).addClass("chk");
                $("#holder").append("<br><input type='checkbox'><label>&nbsp;topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br>");
                $("#holder2").append("<br><input type='checkbox'><label>&nbsp;topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br>");
                $("#holder3").append("<br><input type='checkbox'><label>&nbsp;topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br><input type='checkbox'><label>&nbsp; topics here</label><br>");
            }else{
                $("#holder").html("");
                $("#holder2").html("");
                $("#holder3").html("");
            }
        });
    });


</script>
<script>
$(function() {

$('#chk').multiselect({

includeSelectAllOption: true

});
</script>
@stop