@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    @include('system._components.notifications')
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-account-add"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Number of Manufacture</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
   
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-account-o"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Number of Distributor</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-attachment"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Number of Retailer</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-accounts-list-alt"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Number of Consumer</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="widget widget-tile">
          <div  class="chart sparkline icon-container">
            <div class="icon">
              <span class="mdi mdi-assignment-alert"></span>
            </div>
          </div>
        <div class="data-info">
          <div class="desc">Reports</div>
          <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="" class="number">0</span>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/morrisjs/morris.css')}}"/>
@stop


@section('page-scripts')
<script src="{{asset('assets/lib/countup/countUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/curvedLines.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/chartjs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/raphael/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/morrisjs/morris.min.js')}}" type="text/javascript"></script>


@stop