@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <form method="POST" action="" enctype="multipart/form-data">
  {!!csrf_field()!!}
  <div class="row">
    <div class="col-md-6">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">Mass Upload <strong>Account</strong> Header Form<span class="panel-subtitle">Bulk upload of account. </span></div>
        <div class="panel-body">
            <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
              <label>Upload Data</label>
              <input type="file" name="file" class="form-control" id="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
              @if($errors->first('file'))
              <span class="help-block">{{$errors->first('file')}}</span>
              @endif
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row xs-pt-15">
    <div class="col-xs-6">
        <button type="submit" class="btn btn-space btn-success">Import Account</button>
        <a href="{{route('system.app_user.index')}}" class="btn btn-space btn-default">Cancel</a>
    </div>
  </div>
  </form>
</div>
@stop