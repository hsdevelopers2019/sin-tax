@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  <div class="row">
    <div class="col-md-5">
      @include('system._components.notifications')
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">
          {{$chat->title}}
          <span class="pull-right"><a href="{{route('system.chat.index')}}">Go Back to Group Chats</a></span>
          <span class="panel-subtitle">Chat details.</span></div>
        <div class="panel-body">
            <div class="form-group">
              <label>Title Name</label>
              <p><strong>{{$chat->title}}</strong></p>
            </div>
            <div class="form-group">
              <label>Author</label>
              <p><strong>{{$chat->author?$chat->author->name:"Unknown User"}}</strong></p>
            </div>
            <div class="form-group">
              <label>Date Created</label>
              <p><strong>{{$chat->date_format($chat->created_at)}}</strong></p>
            </div>
            <div class="form-group">
              <label>No. of Participants</label>
              <p><strong>{{$chat->participant->count()}}</strong></p>
            </div>

            <div class="form-group">
              <label>Status</label>
              <p><span class="text-{{Helper::status_badge($chat->status)}}"><strong>{{Str::title($chat->status)}}</strong></span> </p>
            </div>
        </div>
      </div>
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">
          Participants
          <span class="panel-subtitle">List of all participants.</span></div>
        <div class="panel-body">
          <div class="table-responsive" style="max-height: 300px; overflow-y: auto;">
            <table class="table table-hover table-wrapper">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Date Joined</th>
                </tr>
              </thead>
              <tbody>
                @foreach($chat->participant as $index => $participant)
                <tr>
                  <td class="cell-detail">
                    <span>{{$participant->author->name}}</span>
                    <span class="cell-detail-description">{{"ID ".str_pad($participant->user_id, 4, "0", STR_PAD_LEFT)}}</span>
                  </td>
                  <td class="cell-detail">
                    <span>{{$participant->role}}</span>
                  </td>
                  <td class="cell-detail">
                    <span>{{Helper::date_only($participant->created_at)}}</span>
                    <span class="cell-detail-description">{{Helper::date_format($participant->created_at,"h:i A")}}</span>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-7">
      <div class="panel panel-default panel-border-color panel-border-color-success">
        <div class="panel-heading panel-heading-divider">
          Conversation
          <span class="panel-subtitle">List of all conversation.</span></div>
        <div class="panel-body">
          <div class="table-responsive" style="min-height: 400px; max-height: 800px; overflow-y: auto;">
            <table class="table table-hover table-wrapper">
              <thead>
                <tr>
                  <th>Message</th>
                  <th>Sender</th>
                  <th>Date Sent</th>
                </tr>
              </thead>
              <tbody>
                @foreach($conversation as $index => $message)
                <tr>
                  <td class="cell-detail">
                    @if(in_array($message->type, ["image",'file']))
                    <span><a href="{{$message->type == "image" ? "{$message->new_directory}/resized/{$message->filename}" :"{$message->new_directory}/{$message->filename}" }}" target="_blank">{{$message->content}}</a></span>
                    @else
                      @if($message->type == "message")
                      <span>{{$message->content}}</span>
                      @else
                      <span><i>{{$message->content}}</i></span>
                      @endif
                    @endif
                    <span class="cell-detail-description">{{$message->type}}</span>
                  </td>
                  <td class="cell-detail">
                    <span>{{$message->sender->name}}</span>
                    <span class="cell-detail-description">{{"ID ".str_pad($message->sender_user_id, 4, "0", STR_PAD_LEFT)}}</span>
                  </td>
                  <td class="cell-detail">
                    <span>{{Helper::date_only($message->created_at)}}</span>
                    <span class="cell-detail-description">{{Helper::date_format($message->created_at,"h:i A")}}</span>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="panel-footer">
          <div class="pagination-wrapper text-center">
            {!!$conversation->render()!!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop


