<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "System", 
	'as' => "system.", 
	// 'prefix'	=> "admin",
	// 'middleware' => "", 

], function(){

	$this->group(['middleware' => ["web","system.guest"]], function(){
		$this->get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		$this->post('register/{_token?}',['uses' => "AuthController@store"]);
		$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
	});

	$this->group(['middleware' => ["web","system.auth","system.client_partner_not_allowed"]], function(){
		
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['as' => "account."],function(){
			$this->get('p/{username?}',['as' => "profile",'uses' => "AccountController@profile"]);
			$this->group(['prefix' => "setting"],function(){
				$this->get('info',['as' => "edit-info",'uses' => "AccountController@edit_info"]);
				$this->post('info',['uses' => "AccountController@update_info"]);
				$this->get('password',['as' => "edit-password",'uses' => "AccountController@edit_password"]);
				$this->post('password',['uses' => "AccountController@update_password"]);
			});
		});

		$this->group(['middleware' => ["system.update_profile_first"]], function() {

			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);

			$this->group(['prefix' => "system-account", 'as' => "user."], function () {
				$this->get('/',['as' => "index", 'uses' => "UserController@index"]);
				$this->get('create',['as' => "create", 'uses' => "UserController@create"]);
				$this->post('create',['uses' => "UserController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "UserController@edit"]);
				$this->post('edit/{id?}',['uses' => "UserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
			});

			$this->group(['prefix' => "file-manager", 'as' => "file."], function () {
				$this->get('/',['as' => "index", 'uses' => "FileManagerController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FileManagerController@create"]);
				$this->post('create',['uses' => "FileManagerController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FileManagerController@edit"]);
				$this->post('edit/{id?}',['uses' => "FileManagerController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FileManagerController@destroy"]);
			});

			$this->group(['prefix' => "chat", 'as' => "chat."], function () {
				$this->get('/',['as' => "index", 'uses' => "ChatController@index"]);
				$this->get('show/{id?}',['as' => "show", 'uses' => "ChatController@show"]);
			});

			$this->group(['prefix' => "mentorship", 'as' => "mentorship."], function () {
				$this->get('/',['as' => "index", 'uses' => "MentorshipController@index"]);
				$this->get('show/{id?}',['as' => "show", 'uses' => "MentorshipController@show"]);
			});

			$this->group(['prefix' => "page", 'as' => "page."], function () {
				$this->get('/',['as' => "index", 'uses' => "PageController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PageController@create"]);
				$this->post('create',['uses' => "PageController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PageController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PageController@destroy"]);
			});

			$this->group(['prefix' => "category", 'as' => "category."], function () {
				$this->get('/',['as' => "index", 'uses' => "CategoryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "CategoryController@create"]);
				$this->post('create',['uses' => "CategoryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "CategoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "CategoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CategoryController@destroy"]);
			});

			$this->group(['prefix' => "article", 'as' => "article."], function () {
				$this->get('/',['as' => "index", 'uses' => "ArticleController@index"]);
				$this->get('published',['as' => "published", 'uses' => "ArticleController@published"]);
				$this->get('pending',['as' => "pending", 'uses' => "ArticleController@pending"]);

				$this->get('create',['as' => "create", 'uses' => "ArticleController@create"]);
				$this->post('create',['uses' => "ArticleController@store"]);
				$this->get('fcm/{id?}',['as' => "force_notification", 'uses' => "ArticleController@force_notification"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ArticleController@edit"]);
				$this->post('edit/{id?}',['uses' => "ArticleController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ArticleController@destroy"]);
			});

			$this->group(['prefix' => "announcement", 'as' => "announcement."], function () {
				$this->get('/',['as' => "index", 'uses' => "AnnouncementController@index"]);
				$this->get('published',['as' => "published", 'uses' => "AnnouncementController@published"]);
				$this->get('pending',['as' => "pending", 'uses' => "AnnouncementController@pending"]);

				$this->get('create',['as' => "create", 'uses' => "AnnouncementController@create"]);
				$this->post('create',['uses' => "AnnouncementController@store"]);
				$this->get('re-notify/{id?}',['as' => "force_notification", 'uses' => "AnnouncementController@force_notification"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AnnouncementController@edit"]);
				$this->post('edit/{id?}',['uses' => "AnnouncementController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AnnouncementController@destroy"]);
			});



				// --------------------------PRODUCT ---------------------------//

			$this->group(['prefix' => "products", 'as' => "product."], function () {
				$this->get('/',['as' => "index", 'uses' => "ProductController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ProductController@create"]);
				$this->get('show',['as' => "show", 'uses' => "ProductController@show"]);
				
			});

			// --------------------------END OF PRODUCT ---------------------------//

			

				$this->group(['prefix' => "app-user", 'as' => "app_user."], function () {
				$this->get('/',['as' => "index", 'uses' => "AppUserController@index"]);
				$this->get('export',['as' => "export", 'uses' => "AppUserController@export"]);

				$this->get('mentors',['as' => "mentor", 'uses' => "AppUserController@mentors"]);
				$this->get('mentees',['as' => "mentee", 'uses' => "AppUserController@mentees"]);

				$this->get('create',['as' => "create", 'uses' => "AppUserController@create"]);
				$this->post('create',['uses' => "AppUserController@store"]);
				
				$this->get('import',['as' => "import", 'uses' => "AppUserController@import"]);
				$this->post('import',['uses' => "AppUserController@submit_import"]);

				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AppUserController@edit"]);
				$this->post('edit/{id?}',['uses' => "AppUserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AppUserController@destroy"]);
			});


			$this->group(['prefix' => "form", 'as' => "form."], function () {
				$this->get('/',['as' => "index", 'uses' => "FormController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FormController@create"]);
				$this->post('create',['uses' => "FormController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FormController@edit"]);
				$this->post('edit/{id?}',['uses' => "FormController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FormController@destroy"]);
			});

			$this->get('ratings',['as' => "index", 'uses' => "RatingController@index"]);
		});
	});
});