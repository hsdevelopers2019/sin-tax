<?php

namespace App\Laravel\Notifications\Chat;

use App\Laravel\Models\User;
use App\Laravel\Models\Chat;
use App\Laravel\Models\ChatConversation;

use App\Laravel\Notifications\FCMNotification;
use Helper;

class NewFileMessageNotification extends FCMNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,Chat $chat,ChatConversation $message)
    {
        $data = [
            'type' => "CHAT",
            'reference_id' => $chat->id,
            'title' => "{$user->name} in {$chat->title}",
            'content' => "Sent attachment : {$message->content}",
            'thumbnail' => $user->thumbnail,
        ];

        parent::__construct($data);
    }
}
