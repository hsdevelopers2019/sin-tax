<?php

namespace App\Laravel\Notifications\Self\Article;

use App\Laravel\Models\Article;
use App\Laravel\Notifications\SelfNotification;
use Helper;

class NewArticleNotification extends SelfNotification
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $data = [
            'type' => "ARTICLE",
            'reference_id' => $article->id,
            'title' => "Your article has been submitted. We'll inform you once its ready in the public blog.",
            'content' => $article->title,
            'thumbnail' => $article->thumbnail,
        ];

        $this->setData($data);
    }
}
