<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Laravel\Models\Mentorship;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\MentorshipConversation;

class MentorshipReset extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mentorship::truncate();
        MentorshipParticipant::truncate();
        MentorshipConversation::truncate();
    }
}
